/*Nick Alto - CS325 p3 - p.mod*/
/*Constraints*/
var ham_fresh >= 0;
var bellies_fresh >= 0;
var picnics_fresh >= 0;
var bellies_smoked_regular >= 0;
var ham_smoked_regular >= 0;
var picnics_smoked_regular >= 0;
var picnics_smoked_overtime >= 0;
var ham_smoked_overtime >= 0;
var bellies_smoked_overtime >= 0;

maximize max: (8 * ham_fresh) + (14 * ham_smoked_regular) + (11 * ham_smoked_overtime) + (4 * bellies_fresh) + (12 * bellies_smoked_regular) + (7 * bellies_smoked_overtime) + (4 * picnics_fresh) + (13 * picnics_smoked_regular) + (9 * picnics_smoked_overtime);

s.t. ham		: ham_fresh + ham_smoked_regular + ham_smoked_overtime <= 480;
s.t. bellies		: bellies_fresh + bellies_smoked_regular + bellies_smoked_overtime <= 400;
s.t. picnics		: picnics_fresh + picnics_smoked_regular + picnics_smoked_overtime <= 230;
s.t. smokeRegular	: ham_smoked_regular + bellies_smoked_regular + picnics_smoked_regular <= 420;
s.t. smokeOvertime	: ham_smoked_overtime + bellies_smoked_overtime + picnics_smoked_overtime <= 250;

end;

