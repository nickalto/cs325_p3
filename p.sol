Problem:    p
Rows:       6
Columns:    9
Non-zeros:  24
Status:     OPTIMAL
Objective:  max = 10910 (MAXimum)

   No.   Row name   St   Activity     Lower bound   Upper bound    Marginal
------ ------------ -- ------------- ------------- ------------- -------------
     1 max          B          10910                             
     2 ham          NU           480                         480             8 
     3 bellies      NU           400                         400             5 
     4 picnics      NU           230                         230             6 
     5 smokeRegular NU           420                         420             7 
     6 smokeOvertime
                    NU           250                         250             3 

   No. Column name  St   Activity     Lower bound   Upper bound    Marginal
------ ------------ -- ------------- ------------- ------------- -------------
     1 ham_fresh    B            440             0               
     2 bellies_fresh
                    NL             0             0                          -1 
     3 picnics_fresh
                    NL             0             0                          -2 
     4 bellies_smoked_regular
                    B            400             0               
     5 ham_smoked_regular
                    NL             0             0                          -1 
     6 picnics_smoked_regular
                    B             20             0               
     7 picnics_smoked_overtime
                    B            210             0               
     8 ham_smoked_overtime
                    B             40             0               
     9 bellies_smoked_overtime
                    NL             0             0                          -1 

Karush-Kuhn-Tucker optimality conditions:

KKT.PE: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

KKT.PB: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

KKT.DE: max.abs.err = 8.88e-16 on column 4
        max.rel.err = 3.55e-17 on column 4
        High quality

KKT.DB: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

End of output
