/*Nick Alto cs325p3.2*/
/*Variables*/
var gA;
var gB;
var gC;
var opt;

/* function to optimize opt var */
minimize sol: opt;

s.t. x1_high	: 1 - gB <= opt;
s.t. x1_low	: 1 - gB >= -opt;
s.t. y1_high   : (gA * 1) + (gB * 3) - gC <= opt;
s.t. y1_low    : (gA * 1) + (gB * 3) - gC >= -opt;
s.t. x2_high	: 2 - gB <= opt;
s.t. x2_low	: 2 - gB >= -opt;
s.t. y2_high   : (gA * 2) + (gB * 5) - gC <= opt;
s.t. y2_low    : (gA * 2) + (gB * 5) - gC >= -opt;
s.t. x3_high	: 3 - gB <= opt;
s.t. x3_low	: 3 - gB >= -opt;
s.t. y3_high   : (gA * 3) + (gB * 7) - gC <= opt;
s.t. y3_low    : (gA * 3) + (gB * 7) - gC >= -opt;
s.t. x4_high	: 5 - gB <= opt;
s.t. x4_low	: 5 - gB >= -opt;
s.t. y4_high   : (gA * 5) + (gB * 11) - gC <= opt;
s.t. y4_low    : (gA * 5) + (gB * 11) - gC >= -opt;
s.t. x5_high	: 7 - gB <= opt;
s.t. x5_low	: 7 - gB >= -opt;
s.t. y5_high   : (gA * 7) + (gB * 14) - gC <= opt;
s.t. y5_low    : (gA * 7) + (gB * 14) - gC >= -opt;
s.t. x6_high	: 8 - gB <= opt;
s.t. x6_low	: 8 - gB >= -opt;
s.t. y6_high   : (gA * 8) + (gB * 15) - gC <= opt;
s.t. y6_low    : (gA * 8) + (gB * 15) - gC >= -opt;
s.t. x7_high	: 10 - gB <= opt;
s.t. x7_low	: 10 - gB >= -opt;
s.t. y7_high   : (gA * 10) + (gB * 19) - gC <= opt;
s.t. y7_low    : (gA * 10) + (gB * 19) - gC >= -opt;

end;

