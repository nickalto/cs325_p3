Problem:    p2
Rows:       29
Columns:    4
Non-zeros:  85
Status:     OPTIMAL
Objective:  sol = 4.5 (MINimum)

   No.   Row name   St   Activity     Lower bound   Upper bound    Marginal
------ ------------ -- ------------- ------------- ------------- -------------
     1 sol          B            4.5                             
     2 x1_high      B            -10                          -1 
     3 x1_low       NL            -1            -1                         0.5 
     4 y1_high      B           -8.8                          -0 
     5 y1_low       B            0.2            -0               
     6 x2_high      B            -10                          -2 
     7 x2_low       B             -1            -2               
     8 y2_high      B           -6.6                          -0 
     9 y2_low       B            2.4            -0               
    10 x3_high      B            -10                          -3 
    11 x3_low       B             -1            -3               
    12 y3_high      B           -4.4                          -0 
    13 y3_low       B            4.6            -0               
    14 x4_high      B            -10                          -5 
    15 x4_low       B             -1            -5               
    16 y4_high      NU             0                          -0         < eps
    17 y4_low       B              9            -0               
    18 x5_high      B            -10                          -7 
    19 x5_low       B             -1            -7               
    20 y5_high      B           -1.1                          -0 
    21 y5_low       B            7.9            -0               
    22 x6_high      B            -10                          -8 
    23 x6_low       B             -1            -8               
    24 y6_high      B           -4.4                          -0 
    25 y6_low       B            4.6            -0               
    26 x7_high      NU           -10                         -10          -0.5 
    27 x7_low       B             -1           -10               
    28 y7_high      NU             0                          -0         < eps
    29 y7_low       B              9            -0               

   No. Column name  St   Activity     Lower bound   Upper bound    Marginal
------ ------------ -- ------------- ------------- ------------- -------------
     1 gA           B           -8.8                             
     2 gB           B            5.5                             
     3 gC           B             12                             
     4 opt          B            4.5                             

Karush-Kuhn-Tucker optimality conditions:

KKT.PE: max.abs.err = 4.44e-15 on row 28
        max.rel.err = 7.40e-17 on row 3
        High quality

KKT.PB: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

KKT.DE: max.abs.err = 2.06e-33 on column 1
        max.rel.err = 2.06e-33 on column 1
        High quality

KKT.DB: max.abs.err = 3.13e-34 on row 16
        max.rel.err = 3.13e-34 on row 16
        High quality

End of output
